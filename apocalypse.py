import os
import time


def ground_zero():
    """
    Ground zero is where parents are created, children are born and then turned into
    zombies because the parents are forced to ignore their children.
    :return:
    """
    birth = os.fork()

    if birth > 0:
        print("Parent process and id is: " + str(os.getpid()))
        time.sleep(60)
        print("Go on... find a terminal and- ps aux | grep Z")
    else:
        print("Child process and id is: " + str(os.getpid()))
        return 0

ground_zero()
